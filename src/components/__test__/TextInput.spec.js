import React from "react";
import ReactDOM from "react-dom";
import TextInput from "../TextInput";

import { cleanup, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import renderer from "react-test-renderer";
import { MenuItem } from "@mui/material";

afterEach(() => cleanup());
it("renders", () => {
  const div = document.createElement("div");
  ReactDOM.render(<TextInput />, div);
});

it("renders correctly", () => {
  const { queryByTestId } = render(<TextInput label="expected label" />);
  expect(queryByTestId("text-input")).toHaveTextContent("expected label");
});

it("matches simple text input snapshot", () => {
  const tree = renderer
    .create(<TextInput label="expected label" value="value" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it("matches dropdown input snapshot", () => {
  const tree = renderer
    .create(
      <TextInput
        select
        value="value"
        label="expected label"
        child={
          <MenuItem key={"key"} value={"value"}>
            Label
          </MenuItem>
        }
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
