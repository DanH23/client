import React from "react";
import { TextField } from "@mui/material";

export const TextInput = ({
  label,
  error,
  onChange,
  helperText,
  color,
  value,
  child,
  response,
  options,
  ...otherProps
}) => {
  return !child ? (
    <TextField
      data-testid="text-input"
      error={error}
      helperText={helperText}
      id="outlined-basic"
      label={label}
      variant="outlined"
      onChange={onChange}
      value={value}
      {...otherProps}
    />
  ) : (
    <TextField
      label={label}
      value={value}
      onChange={onChange}
      error={error}
      helperText={helperText}
      color={color}
      {...otherProps}
    >
      {child}
    </TextField>
  );
};

export default TextInput;
