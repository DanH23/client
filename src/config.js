export const SERVER_PORT = 5000;
export const SOCKET_PORT = 5001;

export const BASE_URL = `http://localhost`;

export const loglevels = [
  {
    value: "undefined",
    label: "Please select a log level",
  },
  {
    value: "error",
    label: "Error",
  },
  {
    value: "info",
    label: "Info",
  },
  {
    value: "debug",
    label: "Debug",
  },
  {
    value: "incorrect",
    label: "Incorrect log level(Testing)",
  },
];
