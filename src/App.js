import { useState, useEffect, useRef } from "react";
import { makeStyles } from "@mui/styles";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  MenuItem,
  Stack,
} from "@mui/material";
import { changeLogLevel, getWeatherForCity } from "./api/api";
import { Box } from "@mui/system";
import { io } from "socket.io-client";
import { BASE_URL, loglevels, SOCKET_PORT } from "./config";
import TextInput from "./components/TextInput";

function App() {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [cityInput, setCityInput] = useState();
  const [logLevel, setLogLevel] = useState();
  const [cityResponse, setCityResponse] = useState();
  const [cityError, setCityError] = useState({ exists: false, message: "" });
  const [logError, setLogError] = useState({ exists: false, message: "" });
  const [logResponse, setLogResponse] = useState();
  const [socket, setSocket] = useState();
  const isMounted = useRef(false);

  useEffect(() => {
    setIsLoading(true);
    const socket = io(`${BASE_URL}:${SOCKET_PORT}`);
    setSocket(socket);
    socket.on("connect", () => {
      console.log(socket.id);
      setIsLoading(false);
    });
    socket.on("disconnect", () => {
      console.log("Socket was disconected");
    });
    socket.on("data", socketListener);

    return () => socket.close();
  }, []);

  const socketListener = (data) => {
    console.log("Socket message received", data);
    setCityResponse(data);
  };

  const triggerSearch = async () => {
    await socket.emit("stop", socket.id);
    setCityResponse(undefined);
    if (!cityInput || cityInput === "") {
      setCityError({ exists: true, message: "Please enter a city" });
      return;
    }
    setIsLoading(true);
    try {
      const response = await getWeatherForCity(cityInput);
      setCityResponse(response);
    } catch (err) {
      setCityError({ exists: true, message: err });
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const triggerLogChange = async () => {
      if (logLevel !== loglevels[0].value) {
        try {
          const reponse = await changeLogLevel(logLevel);
          setLogResponse(reponse);
        } catch (err) {
          setLogError({ exists: true, message: err });
        }
      }
    };
    if (isMounted.current) {
      setLogError({ exists: false, message: "" });
      triggerLogChange();
    } else {
      isMounted.current = true;
    }
  }, [logLevel]);

  const handleLogChange = async (event) => {
    setLogResponse(undefined);
    setLogLevel(event.target.value);
  };

  const handleInputChange = (event) => {
    setCityError({ exists: false, message: "" });
    setCityInput(event.target.value);
  };

  return (
    <Container direction="column" className={classes.content}>
      <Container className={classes.inputsContent}>
        <Stack spacing={4}>
          <TextInput
            error={cityError.exists}
            helperText={cityError.exists && cityError.message}
            id="outlined-basic"
            label={"Type a city"}
            variant="outlined"
            onChange={handleInputChange}
            value={cityInput}
          />
          <Button onClick={() => triggerSearch()}>Search</Button>
          <TextInput
            select
            label={"Select"}
            value={logLevel}
            onChange={handleLogChange}
            error={logError.exists}
            helperText={
              (logError.exists && logError.message) ||
              (logResponse && logResponse.message)
            }
            color={logResponse && "success"}
            child={loglevels.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          />
        </Stack>
      </Container>
      {!isLoading && cityResponse && (
        <Container direction="column" className={classes.resultContent}>
          <Box className={classes.labels}>
            {cityResponse.timezone && cityResponse.timezone.split("/")[1]}
          </Box>
          <Box className={classes.labels}>{cityResponse.localTime}</Box>
        </Container>
      )}
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </Container>
  );
}

const useStyles = makeStyles({
  content: {
    padding: "0 30px",
  },
  inputsContent: {
    padding: "30px 0",
  },
  resultContent: {
    padding: "10px 0",
  },
  labels: {
    padding: "10px 0",
    textAlign: "center",
  },
});

export default App;
