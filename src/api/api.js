import { BASE_URL, SERVER_PORT } from "../config";

export const httpMethod = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
};

const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
  Authorization: null,
};

export const getWeatherForCity = async (city) => {
  try {
    const response = await fetch(`${BASE_URL}:${SERVER_PORT}/time/${city}`, {
      method: httpMethod.GET,
      headers: headers,
    });
    return handleSuccessResponse(response);
  } catch (err) {
    return handleFailureResponse(err);
  }
};

export const changeLogLevel = async (logLevel) => {
  try {
    const response = await fetch(
      `${BASE_URL}:${SERVER_PORT}/logging/${logLevel}`,
      {
        method: httpMethod.PUT,
        headers: headers,
      }
    );
    return handleSuccessResponse(response);
  } catch (err) {
    return handleFailureResponse(err);
  }
};

const handleSuccessResponse = async (response) => {
  if (response.status >= 400) {
    return handleFailureResponse(response);
  }
  return await response.json();
};

const handleFailureResponse = async (errResponse) => {
  const err = await errResponse.json();
  return Promise.reject(err);
};
