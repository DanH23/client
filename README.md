# Client React APP

A UI with an input text field and a dropdown. The user is able to input a city and the localtime will be displayed once the search button is hit. The user is also able to change the log level of the server by selecting a value from the dropdown.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
